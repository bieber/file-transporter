SET CONFDIR=%~dp0%..\conf
SET LOG_DIR=%~dp0%..\logs
SET CLASSPATH=%~dp0..\*;%~dp0..\lib\*;%CLASSPATH%
SET CFG=%CONFDIR%\server.properties
set MAINCLASS=com.bieber.server.TransporterSetup
echo on
java "-Dfile.transporter.log=%LOG_DIR%" -cp "%CLASSPATH%" "%MAINCLASS%" -c "%CFG%" %*
endlocal
