package com.bieber.common;

import org.apache.commons.lang.StringUtils;

/**
 * Created by bieber on 2015/8/21.
 */
public class Node {

    private String nodeName;
    
    private String host;
    
    private int port;

    private volatile boolean available=false;
    
    private static final String PROTOCOL_SPLIT="://";
    

    public Node(String nodeStr) {
        String[] splits = StringUtils.split(nodeStr,PROTOCOL_SPLIT);
        nodeName = splits[0];
        host = splits[1];
        port=Integer.parseInt(splits[2]);
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {

        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return nodeName+"://"+host+":"+port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        if (nodeName != null ? !nodeName.equals(node.nodeName) : node.nodeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return nodeName != null ? nodeName.hashCode() : 0;
    }
}
