package com.bieber.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by bieber on 2015/8/19.
 */
public class Constants {
    
    public static final String SPLIT="->";
    
    public static final int HEAD_SIZE=18;

    public static final int SERIALIZE_BUFFER_SIZE=1024;

    public static final int SERIALIZE_MAX_BUFFER_SIZE=1024*10;

    public static final String APP_NAME = "transportersrv";
    
    public static final Logger COMMON_LOGGER = LoggerFactory.getLogger("TRANSPORTER-SERVER");

    public static final String DEFAULT_ROOT ="transporters";
    
    public static final String CONFIGURATOR_PATH = "configurators";
    
    public static final String NODES_PATH = "nodes";

    public static final String SHUTDOWN_COMMAND="shutdown";

    public static final String STATISTICS="statistics";
    
    
    
}
