package com.bieber.server;


import com.bieber.common.Constants;

/**
 * Created by bieber on 2015/8/20.
 */
public class TransporterSetup {
    
    public static void main(String[] args){
        Constants.COMMON_LOGGER.info("Starting Transporter server.....");
        Transporter.start(args);
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                if(Transporter.isRunning()){
                    Constants.COMMON_LOGGER.info("Stopping Transporter server.....");
                    Transporter.stop();
                }
            }
        });
    }

}
