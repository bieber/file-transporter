package com.bieber.server.pack;

import com.bieber.server.exception.FileTransportException;

import java.nio.ByteBuffer;

/**
 * Created by bieber on 2015/8/19.
 */
public class FilePackage {
    
    private FileStatus status;

    /**
     * 只有开始传输文件的时候会将文件名传递过来
     */
    private String fileName="";
    /**
     * 文件大小
     */
    private long fileSize;
    /**
     * 只有开始传输文件的时候会将远程节点名传递过来
     */
    private String nodeName="";
    /**
     * 传输的文件内容
     */
    private ByteBuffer content;

    private int contentLength;
    
    private long offset;

    //当文件状态为Erorr的时候则有该字段
    private FileTransportException exception;

    public int getContentLength() {
        return contentLength;
    }

    public void setContentLength(int contentLength) {
        this.contentLength = contentLength;
    }

    public FileTransportException getException() {
        return exception;
    }

    public void setException(FileTransportException exception) {
        this.exception = exception;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public FileStatus getStatus() {
        return status;
    }

    public void setStatus(FileStatus status) {
        this.status = status;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public ByteBuffer getContent() {
        return content;
    }

    public void setContent(ByteBuffer content) {
        this.content = content;
    }

    public void clean(){
        offset=0;
        nodeName= null;
        fileName= null;
        offset=0;
        content=null;
    }

}
