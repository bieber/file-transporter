package com.bieber.server.pack;

/**
 * Created by bieber on 2015/8/19.
 */
public enum FileStatus {
 
    START_WRITE((byte)1), WRITE_CONTENT((byte)2), END_WRITE((byte)4), ERROR((byte)8),IGNORE((byte)16);
    private byte status;
    
    FileStatus(byte status) {
        this.status = status;
    }

    public byte getStatus() {
        return status;
    }

    public static FileStatus valueOf(byte value){
        FileStatus[] constants = FileStatus.class.getEnumConstants();
        for(FileStatus filestatus:constants){
            if((filestatus.status&value)>0){
                return filestatus;
            }
        }
        return ERROR;
    }

}
