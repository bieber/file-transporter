package com.bieber.server.config;

import com.bieber.common.AbstractConfig;

import java.io.File;

/**
 * Created by bieber on 2015/8/19.
 */
public class ServerConfig extends AbstractConfig{

    private String bindAddress;

    private String nodeName;

    private int port=30888;

    private int chunkSize=1024*1024;

    private String charSet="UTF-8";

    private String acceptFileDir =System.getProperty("user.home")+ File.separator+"transporter-server"+File.separator+"/accept";

    private String sendFileDir =System.getProperty("user.home")+ File.separator+"transporter-server"+File.separator+"/send";

    private int workerThreadSize=Runtime.getRuntime().availableProcessors();

    private int serverSocketSndBufSize=65535;

    private int serverSocketRcvBufSize=65535;

    private int clientConnectTimeout=3000;

    private int scanFileDelay=60*1000*10;

    private int scanInitialDelay=1000;

    private long subFileSize=1024*1024*10;//1M

    public String getBindAddress() {
        return bindAddress;
    }

    public void setBindAddress(String bindAddress) {
        this.bindAddress = bindAddress;
    }

    public long getSubFileSize() {
        return subFileSize;
    }

    public void setSubFileSize(long subFileSize) {
        this.subFileSize = subFileSize;
    }

    public int getScanFileDelay() {
        return scanFileDelay;
    }

    public void setScanFileDelay(int scanFileDelay) {
        this.scanFileDelay = scanFileDelay;
    }

    public int getScanInitialDelay() {
        return scanInitialDelay;
    }

    public void setScanInitialDelay(int scanInitialDelay) {
        this.scanInitialDelay = scanInitialDelay;
    }

    public int getClientConnectTimeout() {
        return clientConnectTimeout;
    }

    public void setClientConnectTimeout(int clientConnectTimeout) {
        this.clientConnectTimeout = clientConnectTimeout;
    }

    public int getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(int chunkSize) {
        this.chunkSize = chunkSize;
    }

    public int getServerSocketRcvBufSize() {
        return serverSocketRcvBufSize;
    }

    public void setServerSocketRcvBufSize(int serverSocketRcvBufSize) {
        this.serverSocketRcvBufSize = serverSocketRcvBufSize;
    }

    public int getWorkerThreadSize() {
        return workerThreadSize;
    }

    public void setWorkerThreadSize(int workerThreadSize) {
        this.workerThreadSize = workerThreadSize;
    }

    public int getServerSocketSndBufSize() {
        return serverSocketSndBufSize;
    }

    public void setServerSocketSndBufSize(int serverSocketSndBufSize) {
        this.serverSocketSndBufSize = serverSocketSndBufSize;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getCharSet() {
        return charSet;
    }

    public void setCharSet(String charSet) {
        this.charSet = charSet;
    }

    public String getAcceptFileDir() {
        return acceptFileDir;
    }

    public void setAcceptFileDir(String acceptFileDir) {
        this.acceptFileDir = acceptFileDir;
    }

    public String getSendFileDir() {
        return sendFileDir;
    }

    public void setSendFileDir(String sendFileDir) {
        this.sendFileDir = sendFileDir;
    }

    @Override
    public String toString() {
        return "ServerConfig{" +
                "nodeName='" + nodeName + '\'' +
                ", port=" + port +
                ", chunkSize=" + chunkSize +
                ", charSet='" + charSet + '\'' +
                ", acceptFileDir='" + acceptFileDir + '\'' +
                ", sendFileDir='" + sendFileDir + '\'' +
                ", workerThreadSize=" + workerThreadSize +
                ", serverSocketSndBufSize=" + serverSocketSndBufSize +
                ", serverSocketRcvBufSize=" + serverSocketRcvBufSize +
                ", clientConnectTimeout=" + clientConnectTimeout +
                ", scanFileDelay=" + scanFileDelay +
                ", scanInitialDelay=" + scanInitialDelay +
                ", subFileSize=" + subFileSize +
                '}';
    }
}
