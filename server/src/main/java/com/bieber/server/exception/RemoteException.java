package com.bieber.server.exception;

/**
 * Created by bieber on 2015/8/19.
 */
public class RemoteException extends RuntimeException {


    public RemoteException(Throwable cause) {
        super(cause);
    }
}
