package com.bieber.server.exception;

/**
 * Created by bieber on 2015/8/19.
 */
public enum  ErrorStatus {
    
    ACCEPT_FILE_NOT_FINISHED((short)0),FILE_NOT_FOUND((short)1), ACCEPT_FILE_ERROR((short)2),CURRENT_NODE_IS_SHUTDOWN((short)3);
    
    private short status;
    
    ErrorStatus(short status){
        this.status = status;
    }

    public short getStatus() {
        return status;
    }
}
