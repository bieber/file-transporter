package com.bieber.server.exception;

/**
 * Created by bieber on 2015/8/19.
 */
public class FileTransportException extends Exception {
    
    private ErrorStatus errorStatus;

    private Exception caseBy;

    public FileTransportException(ErrorStatus errorStatus) {
        this.errorStatus = errorStatus;
    }

    public FileTransportException(ErrorStatus errorStatus,Exception e){
        this.errorStatus = errorStatus;
        this.caseBy = e;
    }

    @Override
    public String getMessage() {
        return "ErrorStatus:"+errorStatus+" case by:"+caseBy==null?"":caseBy.getMessage();
    }



    public ErrorStatus getErrorStatus() {
        return errorStatus;
    }
}
