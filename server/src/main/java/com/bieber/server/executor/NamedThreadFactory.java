package com.bieber.server.executor;

import javax.lang.model.element.Name;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by bieber on 2015/8/19.
 */
public class NamedThreadFactory implements ThreadFactory {

    private String threadName;

    private AtomicLong threadIndex = new AtomicLong(0);

    public NamedThreadFactory(String threadName){
        this.threadName = threadName;
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r,threadName+threadIndex.getAndIncrement());
    }
}
