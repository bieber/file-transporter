package com.bieber.server.serialize;

/**
 * Created by bieber on 2015/8/20.
 */
public interface Serializer {

    public byte[] serialize(Object object);

    public<T extends Object> T deSerialize(byte[] bytes,Class<T> type);

}
