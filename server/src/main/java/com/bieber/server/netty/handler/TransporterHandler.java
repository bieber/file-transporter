package com.bieber.server.netty.handler;

import com.bieber.server.pack.FilePackage;
import io.netty.channel.Channel;

/**
 * Created by bieber on 2015/8/19.
 */
public interface TransporterHandler {

    public void doHandle(Channel channel ,FilePackage filePackage);
}
