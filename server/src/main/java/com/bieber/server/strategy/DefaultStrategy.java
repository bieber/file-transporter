package com.bieber.server.strategy;

import com.bieber.common.Constants;
import com.bieber.common.Node;
import com.bieber.server.config.ServerConfig;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

/**
 * Created by bieber on 2015/8/21.
 */
public class DefaultStrategy extends AbstractStrategy {
    
    private ServerConfig config;
    
    public DefaultStrategy(ServerConfig config){
        this.config = config;
    }


    @Override
    protected Node convertNode(String nodeStr) {
        try {
            nodeStr = URLDecoder.decode(nodeStr,config.getCharSet());
            Node node = new Node(nodeStr);
            node.setAvailable(true);
            if(!config.getNodeName().equals(node.getNodeName())&&config.getNodeName().compareTo(node.getNodeName())>0){
                return node;
            }
        } catch (UnsupportedEncodingException e) {
            Constants.COMMON_LOGGER.warn("decode node occur an exception",e);
        }
        return null;
    }
}
