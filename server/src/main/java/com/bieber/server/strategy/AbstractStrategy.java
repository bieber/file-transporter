package com.bieber.server.strategy;

import com.bieber.common.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bieber on 2015/8/21.
 */
public abstract class AbstractStrategy implements SenderStrategy {

    @Override
    public List<Node> convert(List<String> nodes) {
        List<Node> result = new ArrayList<Node>();
        for(String nodeStr:nodes){
            Node node = convertNode(nodeStr);
            if(null!=node){
                result.add(node);
            }
        }
        return result;
    }
    
    protected abstract Node convertNode(String nodeStr);
}
