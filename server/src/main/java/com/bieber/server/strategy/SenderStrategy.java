package com.bieber.server.strategy;

import com.bieber.common.Node;

import java.net.URL;
import java.util.List;

/**
 * Created by bieber on 2015/8/21.
 */
public interface SenderStrategy {
    
    public List<Node> convert(List<String> nodes);
    
}
