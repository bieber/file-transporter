package com.bieber.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Map;

/**
 * Created by bieber on 2015/8/30.
 */
public class MappedTest {

    public static void main(String[] args) throws IOException {
        FileChannel targetChannel = new FileInputStream(new File("F:\\target\\copy.txt")).getChannel();

        File source = new File("F:\\test.txt");
        FileChannel sourceChannel = new FileInputStream(source).getChannel();
        long index=0;
        while(index<source.length()){
            MappedByteBuffer mappedByteBuffer = sourceChannel.map(FileChannel.MapMode.READ_ONLY, index, 10);
            mappedByteBuffer.load();
            mappedByteBuffer.flip();
            MappedByteBuffer writeBuffer = targetChannel.map(FileChannel.MapMode.READ_WRITE, index, index + mappedByteBuffer.remaining());
            writeBuffer.put(mappedByteBuffer.array());
            writeBuffer.flip();
            writeBuffer.force();
            index=index+10;
        }



    }
}
