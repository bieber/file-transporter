package com.bieber.server;

import com.bieber.server.serialize.KryoSerializer;

/**
 * Created by bieber on 2015/8/20.
 */
public class KryoSerializerTest {

    public static void main(String[] args){
        KryoSerializer serializer = new KryoSerializer();
        Student student = new Student();
        student.setName("bieber");
        student.setAge(23);
        byte[] objectBytes = serializer.serialize(student);
        System.out.println(objectBytes.length);
        Object object =   serializer.deSerialize(objectBytes, Object.class);
        System.out.print(object);
    }

    static class Student{
        private String name;

        private Integer age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
