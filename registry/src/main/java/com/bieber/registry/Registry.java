package com.bieber.registry;

import java.util.List;

/**
 * Created by bieber on 2015/8/20.
 */
public interface Registry {
    
    public void staticRegister(String path);

    public void dynamicRegister(String path);
    
    public void staticRegister(String path,boolean ignoreExist);

    public void dynamicRegister(String path,boolean ignoreExist);
    
    public void subscribe(String path,Notification notification);

    public void unregister(String path);

    public void updateData(String path,byte[] data);

    public byte[] getData(String path);

    public List<String> listChildren(String path);

    public void stop();
    
}
