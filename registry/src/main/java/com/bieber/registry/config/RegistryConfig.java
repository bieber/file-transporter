package com.bieber.registry.config;

import com.bieber.common.AbstractConfig;
import com.bieber.common.Constants;

/**
 * Created by bieber on 2015/8/20.
 */
public class RegistryConfig extends AbstractConfig{

    private String zookeeperConnections;


    private String root= Constants.DEFAULT_ROOT;

    private int sessionTimeout = 60*1000;

    private int connectionTimeout = 5*1000;

    private int maxElapsedTime = 30*1000;

    private int sleepMsBetweenRetries = 30*1000;

    public int getMaxElapsedTime() {
        return maxElapsedTime;
    }

    public void setMaxElapsedTime(int maxElapsedTime) {
        this.maxElapsedTime = maxElapsedTime;
    }

    public int getSleepMsBetweenRetries() {
        return sleepMsBetweenRetries;
    }

    public void setSleepMsBetweenRetries(int sleepMsBetweenRetries) {
        this.sleepMsBetweenRetries = sleepMsBetweenRetries;
    }

    public int getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public String getZookeeperConnections() {
        return zookeeperConnections;
    }

    public void setZookeeperConnections(String zookeeperConnections) {
        this.zookeeperConnections = zookeeperConnections;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }
}
