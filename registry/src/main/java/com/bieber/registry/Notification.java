package com.bieber.registry;

import org.apache.curator.framework.recipes.cache.ChildData;

import java.util.List;

/**
 * Created by bieber on 2015/8/20.
 */
public interface Notification {

    public void notify(List<String> children);

}
